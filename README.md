# Shell script to issue / renew a Let's Encrypt Certificate with the DNSAPI for accounts at [Domain Offensive](https://www.do.de) (without reseller account)

1. This script depends on [acme.sh](https://github.com/Neilpang/acme.sh)
2. When issueing a certificate for the first time, it will git clone the acme.sh script (including the dnsapi helper script for Domain Offensive) to ````/root```` and install acme.sh into ````/root/.acme.sh````, then request a certificate (via the dnsapi method of acme.sh), and then install the certificate and key in a defined location and finally restart Apache (and Nginx if needed)
3. Usage: ````letsencrypt-dnsapi-doapi.sh {install|update}````
4. acme.sh installs a cronjob for automatic renewal, so no further 
