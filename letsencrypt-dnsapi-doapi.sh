#!/bin/sh

DOMAIN="example.com"
EMAIL="email@example.com"
ACME="/root/.acme.sh/acme.sh"
KEY_PATH="/etc/apache2/ssl/example.com-LE-key.pem"
FULLCHAIN_PATH="/etc/apache2/ssl/example.com-LE-fullchain.pem"
export DO_LETOKEN="1234ABCD5678"


ISSUE(){
        logger "(Re)Issueing Let's Encrypt Cert with DNSAPI (doapi) method!"
        $ACME --issue --dns dns_doapi -d $DOMAIN 2>&1 > /dev/null

        if [ $? -eq 0 ]; then
                logger "Certificate issued successfully"
        fi
}

INSTALL_CERT(){
        logger "(Re)Installing Let's Encrypt Cert files in Apache / Nginx!"
        $ACME --install-cert -d $DOMAIN \
                --key-file       $KEY_PATH  \
                --fullchain-file $FULLCHAIN_PATH \
                --reloadcmd     "systemctl reload apache2.service"

        if [ $? -eq 0 ]; then
                logger "Certificate installed successfully"
        fi

        systemctl reload nginx.service
}

case "$1" in

        install)
                cd /root
                git clone https://github.com/Neilpang/acme.sh.git
                cd ./acme.sh
                ./acme.sh --install

                $ACME --register-account

                $ACME --update-account --accountemail $EMAIL

                ISSUE

                INSTALL_CERT
        ;;

        update)
                ISSUE

                INSTALL_CERT
        ;;

        *)
                echo "Not enough arguments given!"
                echo "Specify either 'install' or 'update'"
        ;;

esac
